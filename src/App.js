import {useCallback, useEffect, useState} from 'react';

import Button from '@mui/material/Button';
import Dropdown from './components';
import { HiSwitchHorizontal } from 'react-icons/hi';
import TextField from '@mui/material/TextField';

import './App.css';

const CURRENCY_API = 'https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/';

const App = () => {
    const [info, setInfo] = useState([]);
    const [input, setInput] = useState(0);
    const [from, setFrom] = useState("usd");
    const [to, setTo] = useState("amd");
    const [options, setOptions] = useState([]);
    const [output, setOutput] = useState(0);

    const url = `${CURRENCY_API}${from}.json`

    useEffect(() => {
        fetch(url).then(data => data.json())
            .then((res) => {
                console.log(res, 8888)
                setInfo(res[from]);
            })
    }, [from, url]);


    const convert = useCallback(() => {
        setOutput(input * info[to]);
    }, [info, input, to]);

    useEffect(() => {
        setOptions(Object.keys(info));
    }, [info, convert])


    const switcher= () => {
        setFrom(to);
        setTo(from);
    };

    return (
        <div className="App">
            <div className="heading">
                <h1>Currency converter</h1>
            </div>
            <div className="container">
                <div className="left">
                    <h3>Amount</h3>
                    <TextField
                        id="outlined-basic"
                        label="Amount"
                        value={input}
                        variant="outlined"
                        type='number'
                        onChange={(e) => {
                            const { value } = e.target;
                            setInput(value);
                        }}
                    />

                </div>
                <div className="middle">
                    <h3>From</h3>
                    <Dropdown data={options}
                              handleChange={(e) => {
                                  setFrom(e.target.value)
                              }}
                              label='From'
                              value={from}/>
                </div>
                <div className="switch">
                    <HiSwitchHorizontal size="30px" onClick={switcher}/>
                </div>
                <div className="right">
                    <h3>To</h3>
                    <Dropdown data={options}
                              handleChange={e => {
                                  setTo(e.target.value)
                              }}
                              label='To'
                              value={to}/>
                </div>
            </div>
            <div className="result">
                <Button variant="outlined" onClick={() => convert()}>Convert</Button>
                <h2>Result Amount:</h2>
                <p>{(input || 0) + " " + from.toUpperCase() + " = " + (input > 0 ? output.toFixed(2): 0) + " " + to.toUpperCase()}</p>

            </div>
        </div>
    );
}

export default App;
