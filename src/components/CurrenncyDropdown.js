import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import {useMemo} from "react";

export default function Dropdown ( { data, handleChange,  value, label } ) {

    const options = useMemo(() => data, [data])

    return (
        <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">{label}</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={value}
                    label={label}
                    onChange={handleChange}
                >{options?.map(item => <MenuItem key={item} value={item}> {item.toUpperCase()}</MenuItem> )}
                </Select>
            </FormControl>
        </Box>
    );
}
